# Getting Started with React-Boiler-Plate

Please Folow this folder structure.

**--> CONTAINER**


Container folder contains layouts , horizontal , verticle etc. In each folder you have basic wrapper components like header and footer.
The purpose of layout is that you call the header and footer one time for whole app , you dont have to add these on every page.

**--> PAGES**


Pages folder containes each layout folder and there respective pages .

**--> ROUTES**


Route folder contains authRoute which is for route protection.
You have to add routes according to your needs in protected routes or public route in index.js file.

**--> COMPONENTS**


All components should be in components folder like , modals , cards , banner etc .

**--> COMPONENTS --> COMMON**


All react common components should be in common folder like input , button , etc.

**--> CONSTANTS**


Files like data.js should be in constants folder.

**--> HELPERS**


All common js functions should be in common-functions.js file , like get-cuurent-time etc.

**--> STYLES**

 (use scss for better reuseibility)
All common classes should be in global.scss
Try to create reuseable classes , and use proper and easy naming convention.




