import axios from "axios";
axios.interceptors.request.use(function (config) {
    config.headers.token = localStorage.getItem("token");
    return config;
});
export const get = (url, params) => {
    return axios({
        method: "get",
        url: url,
        params: params ? params : {},
    });
};
export const post = (url, body, params) => {
    return axios({
        method: "post",
        url: url,
        params: params ? params : {},
        data: body,
    });
};
export const put = (url, body, params) => {
    return axios({
        method: "put",
        url: url,
        params: params ? params : {},
        data: body,
    });
};
export const deleteMethod = (url, body, params) => {
    return axios({
        method: "delete",
        url: url,
        params: params ? params : {},
        data: body,
    });
};
const axIns = axios.create({
    baseURL: process.env.REACT_APP_IP_ADDRESS || "http://13.250.32.18:3000/v1",
});
axIns.interceptors.request.use((config) => {
    config.headers.token = localStorage.getItem("token");
    return config;
});
export const api = {
    $viaCallback: async (promise, onSuccess = () => { }, onError = () => { }) => {
        const [err, res] = await promise;
        if (err) {
            onError(err);
            return [err, res];
        }
        const res2 = await onSuccess(res);
        return [err, res, res2];
    },
    $get(url, config) {
        return new Promise((resolve) => {
            return axIns
                .get(url, config)
                .then((res) => resolve([null, res.data, res]))
                .catch((err) => resolve([err]));
        });
    },
    $post(url, data, config) {
        return new Promise((resolve) => {
            return axIns
                .post(url, data, config)
                .then((res) => resolve([null, res.data, res]))
                .catch((err) => resolve([err]));
        });
    },
};







