import React from 'react'
import Footer from './Footer'
import Header from './Header'
import Sidebar from './Sidebar'

const HorizontalLayout = () => {
    return (
        <div>
            <Header />
            <Sidebar />
            <Footer />
        </div>
    )
}

export default HorizontalLayout
