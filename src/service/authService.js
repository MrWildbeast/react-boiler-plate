// All user related database operations can be defined here.
import { get, post, put } from "../api/api";
import {
    getCurrentUserUrl,
    getForgotPasswordUrl,
    getLoginUrl,
    getRegisterUrl,
    getResetPasswordUrl,
    getSearchUsersUrl,
    getUserPermissionsUrl,
    getUsersUrl,
} from "../utils/urlUtils";
// import { errorUtils } from "../utils/ErrorUtils";
export const login = (data) => {
    const loginUrl = getLoginUrl();
    return post(loginUrl, data);
};
export const register = (data) => {
    const registerUrl = getRegisterUrl();
    return post(registerUrl, data);
};
export const forgotPassword = async (data) => {
    const forgotPasswordUrl = getForgotPasswordUrl();
    try {
        const response = await post(forgotPasswordUrl, data);
        return response.data;
    } catch (error) {
        // throw new Error(errorUtils.getError(error));
    }
};
export const resetPassword = async (data, token) => {
    const resetUrl = getResetPasswordUrl();
    const queryParam = { token: token };
    try {
        const response = await put(resetUrl, data, queryParam);
        return response.data;
    } catch (error) {
        // throw new Error(errorUtils.getError(error));
    }
};
export const getCurrentUser = async () => {
    const currentUserUrl = getCurrentUserUrl();
    try {
        const response = await get(currentUserUrl);
        return response.data;
    } catch (error) {
        // throw new Error(errorUtils.getError(error));
    }
};
export const getUserPermissions = async () => {
    const userPermissionsUrl = getUserPermissionsUrl();
    try {
        const response = await get(userPermissionsUrl);
        return response.data;
    } catch (error) {
        // throw new Error(errorUtils.getError(error));
    }
};
export const updateUserProfile = async (data) => {
    const usersUrl = getUsersUrl();
    try {
        const response = await put(usersUrl, data);
        return response.data;
    } catch (error) {
        // throw new Error(errorUtils.getError(error));
    }
};
export const searchUsers = async (data) => {
    const usersUrl = getSearchUsersUrl();
    try {
        const response = await get(usersUrl, data);
        return response.data.data;
    } catch (error) {
        // throw new Error(errorUtils.getError(error));
    }
};