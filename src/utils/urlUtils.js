const hostPath =
    process.env.REACT_APP_IP_ADDRESS || "https://blocities-backend.herokuapp.com";
const urls = {
    login: "/user/login",
    register: "/signup",
    resetPassword: "/reset-password",
    forgotPassword: "/forget-password",
    currentUser: "/users/current",
};
// const res = await axios.post(`${BASE_API_URL}/user/signup`, values);
export const getLoginUrl = () => {
    const basePath = urls.login;
    return `${hostPath}${basePath}`;
};
export const getRegisterUrl = () => {
    const basePath = urls.register;
    return `${hostPath}${basePath}`;
};
export const getCurrentUserUrl = () => {
    const basePath = urls.currentUser;
    return `${hostPath}${basePath}`;
};
export const getResetPasswordUrl = () => {
    const basePath = urls.resetPassword;
    return `${hostPath}${basePath}`;
};
export const getForgotPasswordUrl = () => {
    const basePath = urls.forgotPassword;
    return `${hostPath}${basePath}`;
};
export const getOrganizationIndustriesLink = (params) => {
    const basePath = urls.organization;
    return `${hostPath}${basePath}/${params.orgId}`;
};
export const getCompanBrandingUrl = (params) => {
    // console.log(params);
    const basePath = urls.hrOrganization;
    return `${hostPath}${basePath}/${params.orgId}`;
};
export const getOrganizationUsersUrl = (params) => {
    const basePath = urls.hrOrganization;
    return `${hostPath}${basePath}/${params.orgId}/users`;
};
export const getEditOrganizationUsersUrl = (params) => {
    const basePath = urls.hrOrganization;
    return `${hostPath}${basePath}/${params.orgId}/users/${params.userId}`;
};
export const getSubroles = (params) => {
    const basePath = urls.userroles;
    return `${hostPath}${basePath}/${params.roleId}/users-subroles`;
};
export const getOrganizationTeamsUrl = (params) => {
    const basePath = urls.hrOrganization;
    return `${hostPath}${basePath}/${params.orgId}/teams`;
};
export const getDeleteOrganizationTeamsUrl = (params) => {
    const basePath = urls.hrOrganization;
    return `${hostPath}${basePath}/${params.orgId}/teams/${params.teamId}`;
};
export const getAddTeamsUserUrl = (params) => {
    const basePath = urls.hrOrganization;
    return `${hostPath}${basePath}/${params.orgId}/teams/${params.teamId}/user`;
};
export const getEditTeamsUserUrl = (params) => {
    const basePath = urls.hrOrganization;
    return `${hostPath}${basePath}/${params.orgId}/teams/${params.teamId}/user/${params.userId}`;
};
export const getDeleteTeamsUserUrl = (params) => {
    const basePath = urls.hrOrganization;
    return `${hostPath}${basePath}/${params.orgId}/teams/${params.teamId}/user/${params.userId}`;
};
export const getOrganizationQuestionnairesUrl = (params) => {
    const basePath = urls.hrOrganization;
    return `${hostPath}${basePath}/${params.orgId}/questionnaires`;
};
export const getAddCandidateInOrgTeamUrl = (params) => {
    const basePath = urls.hrOrganization;
    return `${hostPath}${basePath}/${params.orgId}/candidates`;
};
export const getOrganizationQuestionnairesUpdateUrl = (params) => {
    const basePath = urls.hrOrganization;
    return `${hostPath}${basePath}/${params.orgId}/questionnaires/${params.questionnairesId}`;
};
export const getOrganizationInvitationUrl = (params) => {
    const basePath = urls.hrInvitation;
    return `${hostPath}${basePath}/${params.userId}/invitations`;
};
export const getOrganizationStatisticsUrl = (params) => {
    const basePath = urls.hrOrganization;
    return `${hostPath}${basePath}/${params.orgId}/summary-statistics`;
};
export const getUserNotifications = () => {
    const basePath = urls.users;
    return `${hostPath}${basePath}/notifications`;
};
export const getSearchUsersUrl = () => {
    const basePath = urls.users;
    return `${hostPath}${basePath}`;
};
export const getUsersUrl = () => {
    const basePath = "/user";
    return `${hostPath}${basePath}`;
};
export const getUserPermissionsUrl = () => {
    const basePath = urls.userPermissions;
    return `${hostPath}${basePath}`;
};