import logo from './logo.svg';
import './App.css';
import { Routes, Route, Outlet, Navigate } from "react-router-dom"
import VerticalLayout from './container/vertical-layout';
import HorizontalLayout from './container/horizontal-layout';
import NoAuthLayout from './container/no-auth-layout';
import Test from './test';
import Login from './pages/no-auth-layout/auth/Login';
import { PublicRouteMiddleware, ProtectedRouteMiddleware } from "./routes/authRoute"
import { protectedRouteList, publicRouteList } from "./routes"


function App() {
  return (
    <>
      <Routes>
        <Route element={<ProtectedRouteMiddleware />}>
          <Route element={<HorizontalLayout />}>
            {protectedRouteList.map((e, i) => (
              <Route key={i} path={e.path} element={<e.element />} />
            ))}
          </Route>
        </Route>
        <Route element={<PublicRouteMiddleware />}>
          <Route element={<NoAuthLayout />}>
            {publicRouteList.map((e, i) => (
              <Route key={i} path={e.path} element={<e.element />} />
            ))}
          </Route>
        </Route>
      </Routes>
    </>
  );
}

export default App;
