import { Outlet, Navigate } from "react-router-dom";
// use redux for protection insted of localstorage
export const ProtectedRouteMiddleware = () => {
    const token = localStorage.getItem("token");
    if (!token) {
        return <Navigate to="/login" replace />;
    }
    return <Outlet />;
};
export const PublicRouteMiddleware = () => {
    const token = localStorage.getItem("token");
    if (token) {
        return <Navigate to="/dashboard" replace />;
    }
    return <Outlet />;
};