
// protected routes import
import Dashboard from "../pages/vertical-layout/dashboard";
//  no auth routes import
import Login from "../pages/no-auth-layout/auth/Login";
import Landingpage from "../pages/no-auth-layout/landing"


//Routes
export const ROUTES = {
   HOME: "/",
   LOGIN: "/login",
   DASHBOARD: "/dashboard",
  };


// protected route list
const protectedRouteList = [
    { path: ROUTES.DASHBOARD, element: Dashboard }
]

// no auth route list 
const publicRouteList = [
    { path: ROUTES.LOGIN, element: Login },
    { path: ROUTES.HOME, element: Landingpage }

]
export {
    protectedRouteList, publicRouteList
}